
■ src
├---■ app
|   ├---■ Components
|   |   ├---■ Client
|   |   |   |--updateInfoClient.component {.ts|.html|.css}  
|   |   |   |--
|   |   |
|   |   ├---■ Admin
|   |   |   |--abcClientAdmin.component {.ts|.html|.css}
|   |   |   |--abcDishesDinks.component {.ts|.html|.css}
|   |   |   |--report.component {.ts|.html|.css}
|   |   |   |--
|   |   |
|   |   ├---■ Home(Dinamic)
|   |   |   |--Menu.component {.ts|.html|.css} 
|   |   |
|   |   ├---■ Navbar (Registrer & Login)
|   |   |   |--Navbar.component {.ts|.html|.css} 
|   |   |
|   |   ├---■ Information
|   |       | information.component {.ts|.html|.css}
|   |   
|   |
|   ├---■ Service
|   |   ├---■ Client
|   |   |   |--updateInfoClient.service {.service}
|   |   |
|   |   ├---■ Admin
|   |       |--abcClientAdmin.service {.service}
|   |       |--abcDishes.service {.service}
|   |       |--report.service {.service}
|   |
|   |--app.component {.ts|.html|.css}
|   
|--index {.ts|.html|.css}