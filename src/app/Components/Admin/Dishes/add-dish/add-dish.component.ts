import { Component, OnInit } from '@angular/core';
import { Dish } from 'src/app/Models/Dish';
import { DishService } from 'src/app/Services/dish.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-add-dish',
  templateUrl: './add-dish.component.html',
  styleUrls: ['./add-dish.component.css']
})
export class AddDishComponent implements OnInit {
  id;
  typeDish;
  desDish;
  nameDish;
  priceDish;
  fileDish;
  dishList = [];
  btnSave=true;
  btnUpdate=false;
  imageUrl: string = "../../../../../assets/imagenmenu-1.jpg";
  fileToUpload: File = null;
  constructor(private _service: DishService,private toastr: ToastrService) { }

  ngOnInit() {
    this.getAll();
  }
  addDish() {
    var dish = new Dish();
    dish.name = this.nameDish;
    dish.price = this.priceDish;
    dish.typeDish = this.typeDish;
    dish.image = this.imageUrl;
    dish.description = this.desDish;

    this._service.addDish(dish).subscribe(data => { this.dishList = data, this.getAll(),this.toastr.success('Platillo agregado') });
  }
  getAll() {
    this._service.getDish().subscribe(data => { this.dishList = data });
  }
  get(dish) {
    this._service.getidDish(dish).subscribe(data => { this.dishList = data });
  }
  preUpdate(dish: Dish) {
    this.priceDish = dish.price;
    this.imageUrl = dish.image;
    this.nameDish = dish.name;
    this.typeDish = dish.typeDish;
    this.desDish = dish.description;
    this.id=dish.id;
    this.btnSave = false;
    this.btnUpdate=true;
  }
  Update(dish: Dish) {
    var dish = new Dish();
    dish.price=this.priceDish;
    dish.image=this.imageUrl;
    dish.name=this.nameDish;
    dish.typeDish=this.typeDish;
    dish.description=this.desDish;
    dish.id=this.id;
    this.btnSave = true;
    this.btnUpdate=false;
    this._service.updateDish(dish).subscribe(data=>{this.getAll(),this.toastr.show('Platillo actualizado'),this.clear()});
  }
  delete(id) {
    //this._service.deleteDish(id).subscribe(data =>{this.dishList=data;this.get()});
  }


  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }
  clear(){
    this.nameDish="";
    this.priceDish="";
    this.typeDish="";
    this.imageUrl="";
    this.desDish="";
  }
}
