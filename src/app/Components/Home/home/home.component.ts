import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CarShopService } from 'src/app/Services/car-shop.service';
import { Producto } from 'src/app/Models/Products';
import { ProductService } from 'src/app/Services/Product.Service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private producto: any;
  private subscription: Subscription;
  private productos: Array<any> = [];
  constructor(private carritoService: CarShopService, private productosService: ProductService) { }

  ngOnInit() {
    this.getCatalogo();
  }

  getCatalogo() {
    this.productosService.getProductos()
      .then(data => {
        this.productos = (data as Array<Producto>).filter(x => x.novedad !== true);
      })
      .catch(error => alert(error));
  }

  addProducto(producto) {
    this.carritoService.addCarrito(producto);
  }


}
