import { Component, OnInit } from '@angular/core';
import { DishService } from '../../../Services/dish.service';
import { Dish } from '../../../Models/Dish';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-dishesusuario',
  templateUrl: './dishesusuario.component.html',
  styleUrls: ['./dishesusuario.component.css']
})
export class DishesusuarioComponent implements OnInit {

  typeDish;
  desDish;
  nameDish;
  priceDish;
  fileDish;
  dishList=[];

  createFormGroup(){
  return new FormGroup({
  typeDish : new FormControl('',[Validators.required]),
  desDish: new FormControl('',[Validators.required]),
  nameDish: new FormControl('',[Validators.required,Validators.maxLength(50)]),
  priceDish: new FormControl('',[Validators.required]),
  fileDish: new FormControl('',[Validators.required]),
  dishList: new FormControl('',[Validators.required,Validators.maxLength(100)]),
  })
    
  }
  constructor(private _service:DishService) { }

  ngOnInit() {
  }
  get(){
    this._service.getDish().subscribe();
  }
  dele(id){
    this._service.deleteDish(id).subscribe();
  }
  update(){
    if(this._service)
    {
    const dish: Dish = new Dish();
    dish.name=this.nameDish;
    dish.price = this.priceDish;
    dish.typeDish = this.typeDish;
    dish.image = this.fileDish;
    this.desDish = this.desDish;
    this._service.updateDish(dish.id).subscribe();
    
    }
  }
  pUpdate(dish){
    this._service.updateDish(dish).subscribe();  }

}
