import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DishesusuarioComponent } from './dishesusuario.component';

describe('DishesusuarioComponent', () => {
  let component: DishesusuarioComponent;
  let fixture: ComponentFixture<DishesusuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DishesusuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DishesusuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
