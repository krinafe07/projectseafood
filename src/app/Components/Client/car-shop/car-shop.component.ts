import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/Models/Products';
import { Subscription } from 'rxjs';
import { CarShopService } from 'src/app/Services/car-shop.service';

@Component({
  selector: 'app-car-shop',
  templateUrl: './car-shop.component.html',
  styleUrls: ['./car-shop.component.css']
})
export class CarShopComponent implements OnInit {


  private carrito: Array<Producto> = [];
  private subscription: Subscription;
  private total: number;
  
  constructor(private carritoService: CarShopService) { }

  ngOnInit() {
    this.carritoService.getCarrito().subscribe(data => {
      console.log(data);
      this.carrito = data;
      this.total = this.carritoService.getTotal();
    },
      error => alert(error));
  }

}
