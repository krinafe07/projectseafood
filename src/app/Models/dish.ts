export class Dish{
    id:number;
    name:string;
    typeDish:string;
    price:number;
    description:string;
    image:string;
}