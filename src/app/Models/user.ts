export class User {
    constructor(
    public Nombre: string,
    public Password: string,
    public Email: string,
    public Apellido: string,
    public Edad: number,
    public Telefono: string,
    public Rol: string
    ){}
    
}