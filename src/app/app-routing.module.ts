import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddDishComponent } from './Components/Admin/Dishes/add-dish/add-dish.component';
import { UpdateDishComponent } from './Components/Admin/Dishes/add-dish/update-dish/update-dish.component';
import { DeleteDishComponent } from './Components/Admin/Dishes/delete-dish/delete-dish.component';
import { AddUserComponent } from './Components/Admin/User/add-user/add-user.component';
import { DeleteUserComponent } from './Components/Admin/User/delete-user/delete-user.component';
import { UpdateUserComponent } from './Components/Admin/User/update-user/update-user.component';
import { HomeComponent } from './Components/Home/home/home.component';
import { DishesusuarioComponent } from './Components/Client/dishesusuario/dishesusuario.component';
import { AdminprincipalComponent } from './Components/Admin/Principal/adminprincipal/adminprincipal.component';
import { ReportComponent } from './Components/Admin/Report/report/report.component';
import { CarShopComponent } from './Components/Client/car-shop/car-shop.component';


const routes: Routes = [
  { path: 'home',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {path:'', component:HomeComponent},
  {path:'home', component:HomeComponent},
  {path:'addDish',component:AddDishComponent},
  {path:'updateDish',component:UpdateDishComponent},
  {path:'deleteDish',component:DeleteDishComponent},
  {path:'addUser',component:AddUserComponent},
  {path:'deleteUser',component:DeleteUserComponent},
  {path:'updateUser',component:UpdateUserComponent},
  {path:'dishesUser',component:DishesusuarioComponent},
  {path:'Adminprincipal', component:AdminprincipalComponent},
  {path:'Reportes',component:ReportComponent},
  {path: 'car-shop', component: CarShopComponent},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
