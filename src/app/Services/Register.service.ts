import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class MenuService {
    public url: string;
    constructor(private http: HttpClient) {
        this.url = "http://localhost:50260/api/";
    }


    register(user_to_register): Observable<any> {
        let headers = new HttpHeaders().set("Content-Type", "application/json");
        return this.http.post(this.url + "users/", user_to_register, { headers: headers });
    }


}