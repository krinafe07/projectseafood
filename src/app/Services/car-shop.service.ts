import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Producto } from '../Models/Products';


@Injectable({
  providedIn: 'root'
})
export class CarShopService {


  private subject: BehaviorSubject<Producto[]> = new BehaviorSubject([]);
  private itemsCarrito: Producto[] = [];

  constructor() { 
    this.subject.subscribe(data => this.itemsCarrito = data);
  }

  addCarrito(producto: Producto) {
    this.subject.next([...this.itemsCarrito, producto]);
  }

  clearCarrito() {
    this.subject.next(null);
  }

  getCarrito(): Observable<Producto[]> {
    return this.subject;
  }

  getTotal() {
    return this.itemsCarrito.reduce((total, producto: Producto) => { return total + producto.precio; }, 0);
  }
}
