import { Injectable } from '@angular/core';
import { Producto } from '../Models/Products';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productos: Array<Producto> = [];

  constructor() { 
    for (let i = 0; i < 30; i++) { // Creamos un conjunto de 20 productos de prueba
      const producto = new Producto();
      producto.codigo = (i + 1);
      producto.titulo = `Ceviche ${i}`;
      producto.descripcion = 'Rico';
      producto.precio = i * 10 + 100;
      producto.novedad = (i < 6); // Marcamos como novedad los 6 primeros
      this.productos.push(producto);
    }
  }

  getProductos() {
    return new Promise((resolve, reject) => {
      if (this.productos.length > 0) {
        resolve(this.productos);
      } else {
        reject('No hay productos disponibles');
      }
    });
  }
}
