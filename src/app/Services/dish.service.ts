import { Injectable } from '@angular/core';
import { Observable,throwError } from 'rxjs';
import { Dish } from '../Models/Dish';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor(private http:HttpClient) { }
  // Para AddDishes
  addDish(dish): Observable<any> {
    return this.http.post<any>("http://localhost:50260/api/dishes", dish).pipe(catchError(this.handleError));
  }

  // Para dishesUser
  updateDish(dish): Observable<any> {
    console.log(dish);
    return this.http.put<any[]>("http://localhost:50260/api/dishes/"+dish.id,dish).pipe(catchError(this.handleError));
  }
  deleteDish(id): Observable<any> {
    return this.http.delete<any>("http://localhost:50260/api/dishes", id).pipe(catchError(this.handleError));
  }

  getDish(): Observable<any> {
    return this.http.get<any>("http://localhost:50260/api/dishes").pipe(catchError(this.handleError));
  }
  getidDish(id): Observable<any> {
    return this.http.get<any>("http://localhost:50260/api/dishes", id).pipe(catchError(this.handleError));
  }
  

  private handleError(error: HttpErrorResponse) {
    let ErrorMessage="";
    if (error.error instanceof ErrorEvent) {
      ErrorMessage ='Client Error:'+ error.error.message;
    } else {

      ErrorMessage=`Server Error: ${error.status} ${error.error} +`;
    }
    // return an observable with a user-facing error message
    //console.log(ErrorMessage);
    return throwError(ErrorMessage);
  };
}

